<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'elearning' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '$<64/4n>a/F.mU*)o(!6GnR5:<hN2/$vL>@A,>VX_W?hqT?newD9pif,pVKqTtB[' );
define( 'SECURE_AUTH_KEY',  '[[v&fJ|:FTtg//EkphwJ_Yd>5aL(4nhxn+]@~&&PHcG6jHn)WZ@N^+OVV=6B/Q0z' );
define( 'LOGGED_IN_KEY',    'tg)rKZWARpZ-?wUQA>F[wR(o3Q4n.#92]PK{cO>!K4)6E~G)?U#_H*Hl6hJBIpLt' );
define( 'NONCE_KEY',        'x4aqlE37y6DG|>wOMP@J8Sv-#*=9jSB.7TyQ0p6 7y=qUHf]0=BuqDOu{yvtDAY?' );
define( 'AUTH_SALT',        'z&`(=`kfyUqZLG anh}E($B{{H^tw8*~z<U-+p-&ttOot*I/giROHQ;_)8&_l$)`' );
define( 'SECURE_AUTH_SALT', 'IR7.u Ea%N[QR-9(](Q#Ax2eFtt5SMRI%+uX28Fga0#!3r%*uas@_2,)^0%A jM|' );
define( 'LOGGED_IN_SALT',   'V<65 2zr,ouUn;,/>gNMDh=n#6^y?z7if~% U#9*CN]Agy7phA}grj2(yYYo;}2t' );
define( 'NONCE_SALT',       'OH*}Pk|YWv;;YmS~jr,Vb2^&|A~-#=*vQ ^1qQU,Vck;:~GMC57XovF$bl<1Vg{+' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
